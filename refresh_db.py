#!/usr/bin/env python3
from utils import database

if __name__ == "__main__":
    DB = database.Database()
    DB.refresh()
    DB.exit_db()
