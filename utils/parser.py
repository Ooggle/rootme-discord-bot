#!/usr/bin/env python3
import os
import random
import json
import requests
from bs4 import BeautifulSoup


def get_score(username):
    if username:
        url = f"https://www.root-me.org/{username}?inc=score"

        req = requests.get(
            url,
            headers={
                "User-agent": "Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0"
            },
        )
        req.close()
        soup = BeautifulSoup(req.text, "html.parser").find_all("h3")
        
        correct_index = 0
        for index, elem in enumerate(soup):
            if "valid.svg" in str(elem):
                correct_index = index
                break

        if len(soup) > 4:
            score = soup[correct_index].text.strip()
            if score.isdigit():
                return int(score)
    return None

def hacker_gif_link(query="hacker"):
    default_gif = "https://media1.giphy.com/media/DBfYJqH5AokgM/200.mp4?cid=790b76114562f78e2d00c0a8692a7bc9a474889235ebd53c"
    api_key = os.getenv("GIPHY_API_KEY")

    if api_key:
        url = f"https://api.giphy.com/v1/gifs/search?api_key={api_key}&q={query}&limit=50"
        try:
            req = requests.get(url)
            json_ouput = json.loads(req.text)

            if "data" in json_ouput:
                data = json_ouput["data"]

                urls = []
                for gif in data:
                    urls.append(gif["url"])

                return random.choice(urls)
        except ConnectionError:
            print("Error : Unable to connect to https://api.giphy.com")

    return default_gif
