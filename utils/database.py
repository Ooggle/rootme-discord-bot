#!/usr/bin/env python3
import sqlite3
from time import sleep
from os.path import isfile
from utils import parser


class Database:
    def __init__(self):
        self.table_name = "rootme"

        self.con = sqlite3.connect("database/rootme.db")
        self.c = self.con.cursor()
        self.create_table()

    def create_table(self):
        """ Create the table """
        sql_command = f"CREATE TABLE {self.table_name} (id INTEGER PRIMARY KEY AUTOINCREMENT,\
         guild_id INTEGER, discord_name VARCHAR (50), username VARCHAR (30), score INTEGER);"

        try:
            self.c.execute(sql_command)
            self.con.commit()
        except sqlite3.OperationalError:
            print(f"The table '{self.table_name}' is already created.")

    def add_user(self, guild_id, discord_name, username, score):
        sql_command = f"INSERT INTO {self.table_name} (guild_id, discord_name, username, score) VALUES\
         ('{guild_id}', '{discord_name}', '{username.lower()}', {score});"

        self.c.execute(sql_command)
        self.con.commit()

    def remove_user(self, guild_id, username):
        sql_command = (
            f"DELETE FROM {self.table_name} WHERE username == '{username.lower()}' AND guild_id = '{guild_id}';"
        )

        self.c.execute(sql_command)
        self.con.commit()

    def update_score(self, username, score):
        sql_command = f"UPDATE {self.table_name} SET score = {score} WHERE username = '{username.lower()}';"

        self.c.execute(sql_command)
        self.con.commit()

    def show_leader_board(self, guild_id):
        sql_command = f"SELECT discord_name, username, score FROM {self.table_name} WHERE guild_id = {guild_id} ORDER BY score DESC;"

        self.c.execute(sql_command)
        self.con.commit()

        return self.c

    def show_db(self, guild_id):
        sql_command = f"SELECT * FROM {self.table_name} WHERE guild_id = {guild_id};"

        self.c.execute(sql_command)
        self.con.commit()

        return self.c

    def drop_table(self):
        sql_command = f"DROP TABLE {self.table_name};"

        self.c.execute(sql_command)
        self.con.commit()

    def get_all_user(self):
        sql_command = f"SELECT DISTINCT username FROM {self.table_name}"
        self.c.execute(sql_command)
        self.con.commit()

        return self.c

    def refresh(self):
        usernames = [c[0] for c in self.get_all_user()]

        for username in usernames:
            print(f"Searching score for: {username}")
            score = parser.get_score(username)

            if score:
                print(f"{username} -> {score}")
                self.update_score(username.lower(), score)
            else:
                print(f"Unable to find the score for: {username}")

            sleep(5)

    def exit_db(self):
        self.con.close()
